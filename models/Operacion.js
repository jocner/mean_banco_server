const mongoose = require('mongoose');

const OperacionSchema = mongoose.Schema({
    saldo: {
        type: Number,
        required: true,
        trim: true
    },
    montotransacion: {
        type: Number,
        required: true,
        trim: true
    },
    operacion: {
        type: String,
        required: true,
        trim: true
    },
    ruttercero: {
        type: String,
        required: false,
        trim: true
    },
    cliente: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Usuario'
    },
    creado: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model('Operacion', OperacionSchema);