const Operacion = require('../models/Operacion');
const Usuario = require('../models/Usuario');
const { validationResult } = require('express-validator');


// Cargar Saldo
exports.cargarSaldo = async (req, res) => {

    // Revisar si hay errores
    const errores = validationResult(req);
    if( !errores.isEmpty() ) {
        return res.status(400).json({errores: errores.array() })
    }
    

    try {
         //extraer el monto a depositar
         const { montotransacion } = req.body;

        //Tipo de Operacion
        const tipo_operacion = 'deposito';

        //consulta
        const saldodisponible = await Operacion.findOne({ cliente : req.usuario.id}).sort({$natural:-1}).limit(1);  
        console.log("monto", saldodisponible);

        if(!saldodisponible){
           
            // Crear un deposito
            const deposito = new Operacion(req.body);

            // Guardar el cliente via JWT
            deposito.cliente = req.usuario.id;

            // 
            deposito.saldo = montotransacion;

            // Guardar tipo de operacion
            deposito.operacion = tipo_operacion;

            // guardamos el deposito
            await deposito.save();
            res.json(deposito);

        }else {

        const resultado = montotransacion + saldodisponible.saldo;
        console.log("resultado", resultado);
        
        // Crear un deposito
        const deposito = new Operacion(req.body);

        // Guardar el cliente via JWT
        deposito.cliente = req.usuario.id;

        // Guardar tipo de operacion
        deposito.operacion = tipo_operacion;

        //Guardar cantidad a depositar
        deposito.saldo = resultado;

        // guardamos el deposito
        await deposito.save();
        res.json(deposito);

        }
        
    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error');
    }

}


exports.retiroSaldo = async (req, res) => {

    // Revisar si hay errores
    const errores = validationResult(req);
    if( !errores.isEmpty() ) {
        return res.status(400).json({errores: errores.array() })
    }
    

    try {
         //extraer el monto a depositar
         const { montotransacion } = req.body;

        //Tipo de Operacion
        const tipo_operacion = 'retiro';

        //consulta
        const saldodisponible = await Operacion.findOne({ cliente : req.usuario.id}).sort({$natural:-1}).limit(1);  
        console.log("monto", saldodisponible);

        if(!saldodisponible){
           
            
            res.json({msg: 'Sin Fondos'});

        }

    
        if(saldodisponible.saldo > montotransacion){

            const resultado = saldodisponible.saldo - montotransacion;
            console.log("resultado", resultado);
            
            // Crear un deposito
            const retiro = new Operacion(req.body);

            // Guardar el cliente via JWT
            retiro.cliente = req.usuario.id;

            // Guardar tipo de operacion
            retiro.operacion = tipo_operacion;

            //Guardar cantidad a depositar
            retiro.saldo = resultado;

            // guardamos el deposito
            await retiro.save();
            res.json(retiro);

            
        }else {

            res.json({msg: 'Saldo Insuficiente'});
        } 
        
        
    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error');
    }


}   

exports.transferencia = async (req, res) => {

    // Revisar si hay errores
    const errores = validationResult(req);
    if( !errores.isEmpty() ) {
        return res.status(400).json({errores: errores.array() })
    }
    

    try {
         //extraer el monto a transferir
         const { montotransacion, ruttercero } = req.body;

        //Tipo de Operacion
        const tipo_operacion = 'transferencia';

        //consulta de usuario
        const registro = await Usuario.findOne({ rut : ruttercero});
        console.log("registro", registro); 

        //consulta de saldo cuenta origen
        const saldodisponible = await Operacion.findOne({ cliente : req.usuario.id}).sort({$natural:-1}).limit(1);  
        console.log("monto", saldodisponible);

        const cuentadestino = await Operacion.findOne({ cliente : registro._id}).sort({$natural:-1}).limit(1);
        console.log("cuenta destino", cuentadestino); 

            if(saldodisponible.saldo > montotransacion){

                const resultado = saldodisponible.saldo - montotransacion;
                console.log("resultado", resultado);
                
                // Crear un deposito
                const transferencia = new Operacion(req.body);

                // Guardar el cliente via JWT
                transferencia.cliente = req.usuario.id;

                // Guardar tipo de operacion
                transferencia.operacion = tipo_operacion;

                //Guardar cantidad a transferir
                transferencia.saldo = resultado;

                // guardamos el deposito
                await transferencia.save();
            //    res.json(transferencia);
            

            if(!cuentadestino){
           
                // Crear un deposito
                const abono = new Operacion(req.body);
    
                // Guardar el cliente via JWT
                abono.cliente = registro._id;
    
                // 
                abono.saldo = montotransacion;
    
                // Guardar tipo de operacion
                abono.operacion = tipo_operacion;
    
                // guardamos el deposito
                await abono.save();
                res.json(abono);
    
            }

            if (cuentadestino){

                const resultado2 = saldodisponible.saldo + montotransacion;

                // Crear un deposito
                const abono = new Operacion(req.body);
    
                // Guardar el cliente via JWT
                abono.cliente = registro._id;
    
                // 
                abono.saldo = montotransacion;
    
                // Guardar tipo de operacion
                abono.operacion = tipo_operacion;

                //Guardar cantidad a depositar
                abono.saldo = resultado2;
    
                // guardamos el deposito
                await abono.save();
                res.json(abono);

             }
                 

        }else {
            res.json({msg: 'Saldo Insuficiente'});
        }
            
        } catch (error) {
            console.log(error);
            res.status(500).send('Hubo un error');
        }
}  


exports.movimientos = async (req, res) => {

   // Revisar si hay errores
//    const errores = validationResult(req);
//    if( !errores.isEmpty() ) {
//        return res.status(400).json({errores: errores.array() })
//    } 

   try {

   // const usuario = await Operacion.find({ cliente : req.usuario.id});

    const consulta = await Operacion.find({ cliente : req.usuario.id});

    res.json({ consulta });

   } catch (error) {
       console.log(error);
   }

}